# StockPiCLI
The command line version of the StockPi project (see also StockpiApp for desktop app)

## Installation
Install Linux, install Gambas3
Use git pull and open in Gambas3 IDE.
Get the library StockpiLib as library in this gambas project.

## Usage
use in the command line : ./stockpicli and follow the help instructions.
Needs a database and the access settings in the ./config/gambas3/stockpicli.conf file.

## Support
Radio Show, Issue tracker, ..

## Roadmap
Must become the Stockpi command line app in Linux, to at least question data from the database, in the future also add data.

## Contributing
Currently we werk together on this project in the radioshow webgang on Radio Centraal, any help welcome.

## Authors and acknowledgment
Wim.webgang, Marthe, Silvia

## License
GPL v3

## Project status
Active (starting up)
