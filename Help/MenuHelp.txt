Menu Help
---------
Short:
A : show About
B : show liB changes
C : show Changes 

(D : start Desktop version - planned)

H : Help (show [general] help text)
I : info (of the application/platform)

L : Log (show logfile)
M : switch metadata on/off (show record creation and update user and datetime)
S : SQL of database structure

? : Whats new?
! : License
# : show history of commands

Q : quit - leave the application

 Command examples:
 - - - - - - - - - 
list part       (list all parts)
